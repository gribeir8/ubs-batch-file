package com.ubs.test.ubsbatchfile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.ubs.test.ubsbatchfile.controller.Sale;
import com.ubs.test.ubsbatchfile.entity.Item;
import com.ubs.test.ubsbatchfile.entity.ItemRepository;
import com.ubs.test.ubsbatchfile.service.ItemService;

@SpringBootTest
@ActiveProfiles("test")
class UbsBatchFileApplicationTests {

	@Autowired
	ItemRepository itemRepo;
	@Autowired
	ItemService service;
	
	@Test
	void serviceTest() throws ParseException {
		
		loadData();

		List<Sale> vendas = this.service.assembleSales("EMMS", 2);

		Sale venda1 = vendas.get(0);
		assertEquals(venda1.getItems().size()>0, true);
		assertEquals(venda1.getTotalQuantity(), 135);
		assertEquals(750.92, (double)Math.round(venda1.getTotalVolumePrice()*100)/100);
		assertEquals(5.56, (double)Math.round(venda1.getMeanPrice()*100)/100);

		Sale venda2 = vendas.get(1);
		assertEquals(venda2.getItems().size()>0, true);
		assertEquals(venda2.getTotalQuantity(), 135);
		assertEquals(749.27, (double)Math.round(venda2.getTotalVolumePrice()*100)/100);
		assertEquals(5.55, (double)Math.round(venda2.getMeanPrice()*100)/100);
	}

	private void loadData() throws ParseException {
		List<Item> list = new ArrayList<Item>();
		Item item = new Item();
		item.setIndustry("industry");
		item.setOrigin("CA");
		item.setPrice("3.75");
		item.setProduct("EMMS");
		item.setQuantity(74);
		item.setType("X");
		list.add(item);
		item = new Item();
		item.setIndustry("industry");
		item.setOrigin("CA");
		item.setPrice("5.39");
		item.setProduct("EMMS");
		item.setQuantity(36);
		item.setType("X");	
		list.add(item);
		item = new Item();
		item.setIndustry("industry");
		item.setOrigin("CA");
		item.setPrice("5.80");
		item.setProduct("EMMS");
		item.setQuantity(99);
		item.setType("X");
		list.add(item);
		item = new Item();
		item.setIndustry("industry");
		item.setOrigin("CA");
		item.setPrice("7.45");
		item.setProduct("EMMS");
		item.setQuantity(61);
		item.setType("X");
		list.add(item);
		this.itemRepo.saveAll(list);
	}
	
}

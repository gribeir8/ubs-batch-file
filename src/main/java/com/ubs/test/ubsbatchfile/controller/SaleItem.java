package com.ubs.test.ubsbatchfile.controller;

public class SaleItem {

	private String product;
	private int quantity;
	private double unitPrice;
	private double volumePrice;
	
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getVolumePrice() {
		return volumePrice;
	}
	public void setVolumePrice(double volumePrice) {
		this.volumePrice = volumePrice;
	}
	
	
}

package com.ubs.test.ubsbatchfile.controller;

import java.util.List;

public class Sale {

	private String store;
	private List<SaleItem> items;
	private int totalQuantity;
	private double totalVolumePrice;
	private double meanPrice;
	
	
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public List<SaleItem> getItems() {
		return items;
	}
	public void setItems(List<SaleItem> items) {
		this.items = items;
	}
	public int getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public double getTotalVolumePrice() {
		return totalVolumePrice;
	}
	public void setTotalVolumePrice(double totalVolumePrice) {
		this.totalVolumePrice = totalVolumePrice;
	}
	public double getMeanPrice() {
		return meanPrice;
	}
	public void setMeanPrice(double meanPrice) {
		this.meanPrice = meanPrice;
	}
	
	
}

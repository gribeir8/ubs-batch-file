package com.ubs.test.ubsbatchfile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ubs.test.ubsbatchfile.service.ItemService;

@RestController
@RequestMapping("/items")
public class ItensController {

	@Autowired
	ItemService service;
	
	@GetMapping(value = "/sales")
	public List<Sale> getSales(@RequestParam("produto") String produto
			,@RequestParam("qtLojas") int qtLojas) {

		return service.assembleSales(produto, qtLojas);
	}
	
}

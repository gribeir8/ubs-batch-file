package com.ubs.test.ubsbatchfile.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubs.test.ubsbatchfile.controller.Sale;
import com.ubs.test.ubsbatchfile.controller.SaleItem;
import com.ubs.test.ubsbatchfile.entity.Item;
import com.ubs.test.ubsbatchfile.entity.ItemRepository;

@Service
public class ItemService {

	@Autowired
	ItemRepository itemRepo;
	
	public List<Sale> assembleSales(String produto, int qtLojas) {
		
		if(qtLojas<1) {
			return null;
		}
		
		List<Sale> vendas = new ArrayList<Sale>();
		for(int i=0; i<qtLojas; i++) {
			Sale venda = new Sale();
			vendas.add(venda);
			venda.setStore("store "+(i+1));
			venda.setItems(new ArrayList<SaleItem>());
		}
		
		Iterable<Item> items = this.itemRepo.findAllByProduct(produto);
		//System.out.println("items: "+ items);

		items.forEach(item -> {
			
			int div = item.getQuantity()/qtLojas;
			int mod = item.getQuantity()%qtLojas;
			if(div > 0) {
				//distribui itens
				for(int i=0; i<qtLojas; i++) {
					Sale venda = vendas.get(i);
					addSaleItem(item, div, venda);
				}
				//distribui resto
				for(int k=0; k<mod; k++) {
					
					int lessIndex = getIndexWithLessTotal(vendas);

					Sale venda = vendas.get(lessIndex);
					addSaleItem(item, 1, venda);
				}
			}else {
				//distribui quantidade inferior ao nr. lojas
				for(int j=0; j<item.getQuantity(); j++) {

					int lessIndex = getIndexWithLessTotal(vendas);
					
					Sale venda = vendas.get(lessIndex);
					addSaleItem(item, 1, venda);				
				}
			}
		});
		
		return vendas;
	}

	//obtém indice da loja com menos quantidade
	private int getIndexWithLessTotal(List<Sale> vendas) {
		int lessIndex = 0;
		int lastTotal = 0;
		for(int i=0; i<vendas.size(); i++) {
		
			if(vendas.get(i).getTotalQuantity()<lastTotal) {
				lessIndex = i;
			}
			lastTotal = vendas.get(i).getTotalQuantity();
		}
		return lessIndex;
	}

	private void addSaleItem(Item item, int quantity, Sale venda) {
		SaleItem saleItem = new SaleItem();
		saleItem.setProduct(item.getProduct());
		saleItem.setQuantity(quantity);
		saleItem.setUnitPrice(item.getPriceVal());
		saleItem.setVolumePrice(saleItem.getQuantity()*saleItem.getUnitPrice());
		venda.getItems().add(saleItem);
		//calcula totais
		venda.setTotalQuantity(venda.getTotalQuantity()+saleItem.getQuantity());
		venda.setTotalVolumePrice(venda.getTotalVolumePrice()+saleItem.getVolumePrice());
		venda.setMeanPrice(venda.getTotalVolumePrice()/venda.getTotalQuantity());
	}

	private Sale[] getSalesSample() {
		
		Sale venda = new Sale();
		venda.setItems(new ArrayList<SaleItem>());
		SaleItem sItem = new SaleItem();
		venda.getItems().add(sItem);
		sItem.setProduct("produto 1");
		sItem.setQuantity(10);
		sItem.setUnitPrice(70);
		sItem.setVolumePrice(700);
		venda.setStore("lojas 1");
		venda.setTotalQuantity(10);
		venda.setTotalVolumePrice(700);
		venda.setMeanPrice(venda.getTotalVolumePrice()/venda.getTotalQuantity());
		Sale[] vendas = new Sale[]{venda};
		return vendas;
	}
}

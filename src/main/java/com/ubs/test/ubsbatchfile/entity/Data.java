package com.ubs.test.ubsbatchfile.entity;

import java.util.Arrays;

public class Data {

	private Item[] data;

	public Item[] getData() {
		return data;
	}

	public void setData(Item[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Data [data=" + Arrays.toString(data) + "]";
	}
	
	
}

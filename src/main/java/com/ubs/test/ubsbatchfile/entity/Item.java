package com.ubs.test.ubsbatchfile.entity;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

@Entity
@Table(indexes = {@Index(name = "unique_row"
, columnList = "product,quantity,price,type,industry,origin"
, unique = true)})
public class Item {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String product;
	private int quantity;
	//TODO parse price string $0.00
	private String price;
	private double priceVal;
	private String type;
	private String industry;
	private String origin;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getPriceVal() {
		return priceVal;
	}
	public void setPriceVal(double priceVal) {
		this.priceVal = priceVal;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) throws ParseException {

		String value = price.replace("$", "");
		if(!StringUtils.isEmpty(value)) {
			
			this.priceVal = NumberFormat.getInstance(Locale.US).parse(value).doubleValue();
		}

		this.price = price;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	@Override
	public String toString() {
		return "\n Item [id=" + id + ", product=" + product + ", quantity=" + quantity + ", price=" + price + ", priceVal="
				+ priceVal + ", type=" + type + ", industry=" + industry + ", origin=" + origin + "]";
	}
	
	
}

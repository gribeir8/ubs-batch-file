package com.ubs.test.ubsbatchfile.entity;

import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {

	public Iterable<Item> findAllByProduct(String product);
	
}

package com.ubs.test.ubsbatchfile;

import java.io.File;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;
import org.springframework.transaction.annotation.Transactional;

import com.ubs.test.ubsbatchfile.entity.Data;
import com.ubs.test.ubsbatchfile.entity.ItemRepository;

@SpringBootApplication
@ComponentScan(basePackages = { "com.ubs.test.ubsbatchfile", "com.ubs.test.ubsbatchfile.controller" , "com.ubs.test.ubsbatchfile.config"})
public class UbsBatchFileApplication {

	public String INPUT_DIR = "INPUT";
    public String OUTPUT_DIR = "SUCCESS";
    public String ERROR_DIR = "ERROR";
    public String FILE_PATTERN = "*.json";
    
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext ctx = new SpringApplication(UbsBatchFileApplication.class).run(args);
        System.out.println("Hit Enter to terminate");
        System.in.read();
        ctx.close();
      }
	
	   @Bean
	    public IntegrationFlow flow() {
		   
           makeDir(OUTPUT_DIR);
           makeDir(ERROR_DIR);
           System.out.println("Polling files on dir: " + INPUT_DIR);
	        return IntegrationFlows.from(
	                    Files.inboundAdapter(new File(INPUT_DIR))
	                    .autoCreateDirectory(true)
	                    .patternFilter(FILE_PATTERN)
	                    	, e -> e.poller(Pollers.fixedDelay(1000)))
	        		.enrichHeaders(h -> h.headerExpression(FileHeaders.ORIGINAL_FILE, "payload"))
	                .transform(Transformers.fromJson(Data.class))
	                .handle("processor", "process", e -> e.advice(advice()))
	                .get();
	    }

		private void makeDir(String name) {
			File dir = new File(name);
			if(!dir.exists()) {
				dir.mkdir();
			}
		}

	    @Bean
	    public Processor processor() {
	        return new Processor();
	    }

	    @Bean
	    public AbstractRequestHandlerAdvice advice() {
	        return new AbstractRequestHandlerAdvice() {
	        	
	            @Override
	            protected Object doInvoke(ExecutionCallback callback, Object target, Message<?> message) {
	                File file = message.getHeaders().get(FileHeaders.ORIGINAL_FILE, File.class);
                    System.out.println("Processing file: " + file.getName());
	                try {
	                    Object result = callback.execute();
	                    file.renameTo(new File(OUTPUT_DIR, file.getName()));
	                    System.out.println(file.getName()+" moved to "+OUTPUT_DIR+" after success");
	                    return result;
	                }
	                catch (Exception e) {
	                    file.renameTo(new File(ERROR_DIR, file.getName()));
	                    System.out.println(file.getName()+" moved to \"+ERROR_DIR+\" after failure");
	                    throw e;
	                }
	            }
	        };
	    }

	    public static class Processor {

	        @Autowired
	        ItemRepository itemRepo;

	        @Transactional
	        public void process(Data in) {
	            
	        	System.out.println(in.toString());
	        	this.itemRepo.saveAll(Arrays.asList(in.getData()));
	        }

	    }	
	
}

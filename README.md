# README #

Steps necessários para rodar a aplicação.

### What is this repository for? ###

* Teste Java UBS

### How do I get set up? ###

* mvn install
* java -jar target/ubs-batch-file-0.0.1-SNAPSHOT.jar
* jogar os arquivos .json na pasta INPUT
* os arquivos processados serão jogados na pasta SUCESSO
* os dados serão salvos no h2 (ver <a href="http://localhost:8080/ubs-batch-file/v1/h2" >console h2</a>)
* para testar o serviço chamar no endpoint:
* http://localhost:8080/ubs-batch-file/v1/items/sales?produto=EMMS&qtLojas=2


